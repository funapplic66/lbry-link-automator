# LBRY Link Automator

Automatically ensures your URLs open in the LBRY app (not annoyingly on a browser) even if you use open.lbry.com links.

## How to use

The code is written to not use any dependencies, like jQuery. Just open the `index.html` file and use the code inside `<script>` tag, paste it before `</body>` line. Then use [lpack-bash](https://gitlab.com/funapplic66/lpack-bash) or [lbry-format](https://github.com/lbryio/lbry-format) to get it on LBRY.